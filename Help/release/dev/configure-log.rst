Configure Log
-------------

* CMake now writes a YAML log of configure-time checks.
  See the :manual:`cmake-configure-log(7)` manual.
